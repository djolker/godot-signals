extends MeshInstance

signal my_signal

# Called when the node enters the scene tree for the first time.
func _ready():
	# Sends a signal to the connected GET object
	emit_signal("my_signal")
	# Sends a signal to ALL GET objects
	get_tree().call_group("gets", "groupSignal")




